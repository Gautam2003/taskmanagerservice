﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManaager.Entities;
using TaskManager.DAL;

namespace TaskManager.BAL
{
    
    public class TaskBusiness
    {

        public List<Task> GetAllTasks()
        {
            using (TaskContext _db = new TaskContext())
            {
                return _db.Tasks.ToList();
            }
        }
        public void AddTask(Task newTask)
        {
            using(TaskContext _db = new TaskContext())
            {
                _db.Tasks.Add(newTask);
                _db.SaveChanges();
            }
        }

        public Task GetTaskByID(string id)
        {
            using (TaskContext _db = new TaskContext())
            {
                List<Task> allTasks = (from task1 in _db.Tasks
                                       select task1).ToList();
                Task task = allTasks.Where(a => a.Task_ID == id).FirstOrDefault();
                return task;
            }
            
        }

        public void UpdateTask(Task taskToUpdate)
        {
            using (TaskContext _db = new TaskContext())
            {
                Task updateTask = _db.Tasks.FirstOrDefault(a => a.Task_ID == taskToUpdate.Task_ID);
                if (updateTask != null)
                {
                    _db.Tasks.Remove(updateTask);
                    _db.Tasks.Add(taskToUpdate);
                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception(string.Format("Task - {0} not found", taskToUpdate.TaskName));
                }
            }
        }


        public bool DeleteTask(string id)
        {
            using (TaskContext _db = new TaskContext())
            {
                Task taskToDelete = _db.Tasks.FirstOrDefault(a => a.Task_ID == id);
                if(taskToDelete != null)
                {
                    _db.Tasks.Remove(taskToDelete);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Task - {0} not found", taskToDelete.TaskName));
                }
            }
        }
    }
    

    
}
