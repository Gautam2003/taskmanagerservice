﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TaskManaager.Entities
{
    [Table("Task")]
    public class Task
    {
        [Key]
        public string Task_ID { get; set; }
        public string TaskName { get; set; }
        public int Parent_ID { get; set; }
        public int? Priority { get; set; }

        [Column(TypeName = "Date")]
        public DateTime? StartDate { get; set; }

        [Column(TypeName = "Date")]
        public DateTime? EndDate { get; set; }
        public string  Status { get; set; }

        public static implicit operator Task(List<Task> v)
        {
            throw new NotImplementedException();
        }
    }
}
