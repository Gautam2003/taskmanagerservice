﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TaskManaager.Entities;

namespace TaskManager.DAL
{
    public class TaskContext : DbContext
    {
        public TaskContext() : base("name=TaskConnect")
        {
        }
        public DbSet<Task> Tasks { get; set; }

    }
}
