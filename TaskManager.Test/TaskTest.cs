﻿using NUnit.Framework;
using TaskManaager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using TaskManager.API.Controllers;

namespace TaskManager.Test
{
    [TestFixture]
    public class TaskTest
    {
        [Test]
        public void Test_GetAllTasks()
        {
            TaskController tc = new TaskController();
            var result = tc.GetAllTasks();
            var actual = result as OkNegotiatedContentResult<List<Task>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);



        }

        [Test]
        public void Test_GetTaskByID()
        {
            TaskController tc = new TaskController();
            var result = tc.GetAllTasks();
            var fresult = result as OkNegotiatedContentResult<List<Task>>;
            Task task = new Task();
            if (result!=null)
            {
                task = fresult.Content.FirstOrDefault();
            }
            IHttpActionResult getTaskID = tc.GetTaskByID(task.Task_ID);
            var actual = getTaskID as OkNegotiatedContentResult<Task>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }


        [Test]
        public void Test_PostTask()
        {
            TaskController tc = new TaskController();
            Task taskToAdd = new Task();
            taskToAdd.TaskName = "Task from Unit Test Project";
            taskToAdd.Priority = 50;
            taskToAdd.StartDate = DateTime.Now;
            taskToAdd.EndDate = DateTime.Now.AddDays(2);
            IHttpActionResult result = tc.PostTask(taskToAdd);
            var actual = result as OkNegotiatedContentResult<Task>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }

        [Test]
        public void Test_UpdateTask()
        {
            TaskController tc = new TaskController();
            Task taskToAdd = new Task();
            taskToAdd.Task_ID = "37edb4b5-22ad-46bc-8f1c-cf38e972d3c1";
            taskToAdd.TaskName = "Task updated from Unit Test Project";
            taskToAdd.Priority = 50;
            taskToAdd.StartDate = DateTime.Now;
            taskToAdd.EndDate = DateTime.Now.AddDays(5);
            IHttpActionResult result = tc.UpdateTask(taskToAdd);
            var actual = result as OkNegotiatedContentResult<Task>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }

        
    }
}
