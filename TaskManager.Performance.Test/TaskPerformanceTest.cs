﻿using NBench;
using TaskManager.API.Controllers;
using TaskManaager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskManager.Performance.Test
{
    public class TaskPerformanceTest
    {
        [PerfBenchmark(Description = "Performace Test for GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void GetAllTasks_PassingTest()
        {
            TaskController controller = new TaskController();
            controller.GetAllTasks();
        }

        [PerfBenchmark(Description = "Performace Test for GET (By ID)", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void GetTaskByID_PassingTest()
        {
            TaskController tc = new TaskController();
            tc.GetTaskByID("629b0e53-5bee-432c-9909-1360d5f8b2bd");
        }

        [PerfBenchmark(Description = "Performace Test for POST", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void PostTask_PassingTest()
        {
            TaskController tc = new TaskController();
            Task taskToAdd = new Task()
            {
                TaskName = "Task from Unit Test Project",
                Priority = 50,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(2)
            };
            tc.PostTask(taskToAdd);

        }

        [PerfBenchmark(Description = "Performace Test for UPDATE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void UpdateTask_PassingTest()
        {
            TaskController tc = new TaskController();
            Task taskToAdd = new Task();
            taskToAdd.Task_ID = "005a3b4e-23b6-474e-bce1-4a994a5cc2d8";
            taskToAdd.TaskName = "Task from Unit Test Project";
            taskToAdd.Priority = 50;
            taskToAdd.StartDate = DateTime.Now;
            taskToAdd.EndDate = DateTime.Now.AddDays(5);
            tc.UpdateTask(taskToAdd);

        }
    }
}
